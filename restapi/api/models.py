from django.db import models

# Create your models here.
class PropertyModel(models.Model):
    id = models.AutoField(primary_key=True,null=False)
    title = models.CharField(max_length=255,null=False)
    address = models.TextField(null=False)
    description = models.TextField(null=False)
    created_at = models.DateTimeField(auto_now_add=True,null=False)
    updated_at = models.DateTimeField(auto_now_add=True,null=False)
    disabled_at = models.DateTimeField(auto_now_add=True, null = True)
    status = models.CharField(max_length=255,null=False)

    def __str__(self):
        return self


class ActivityModel(models.Model):
    id = models.AutoField(primary_key=True,null=False)
    property_id = models.ForeignKey(PropertyModel,null=False, on_delete=models.CASCADE)
    schedule = models.DateTimeField(auto_now_add=True,null=False)
    title = models.CharField(max_length=255,null=False)
    created_at = models.DateTimeField(auto_now =True,null=False)
    updated_at = models.DateTimeField(auto_now_add=True,null=False)
    status = models.CharField(max_length=255,null=False)
    
    def __str__(self):
        return self 

class Survey(models.Model):
    id = models.AutoField(primary_key=True,null=False)
    activity_id = models.ForeignKey(ActivityModel,null=False, on_delete=models.CASCADE)
    answer = models.TextField(null=False)
    created_at = models.DateTimeField(auto_now =True,null=False)
    
    def __str__(self):
        return self