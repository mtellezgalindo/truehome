from rest_framework import routers
from .views import property_viewset

router = routers.DefaultRouter()
router.register('property',property_viewset)