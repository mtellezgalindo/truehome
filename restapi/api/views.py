from django.shortcuts import render
from rest_framework import viewsets
from .serializers import property_serlializer
from .models import PropertyModel
# Create your views here.


class property_viewset(viewsets.ModelViewSet):
    queryset = PropertyModel.objects.all()
    serializer_class = property_serlializer